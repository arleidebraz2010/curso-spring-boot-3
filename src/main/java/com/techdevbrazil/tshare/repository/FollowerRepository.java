package com.techdevbrazil.tshare.repository;

import com.techdevbrazil.tshare.enitity.Follower;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FollowerRepository extends JpaRepository<Follower, Long> {

}
