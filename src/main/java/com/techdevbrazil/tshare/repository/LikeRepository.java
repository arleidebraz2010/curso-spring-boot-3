package com.techdevbrazil.tshare.repository;

import com.techdevbrazil.tshare.enitity.Like;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeRepository extends JpaRepository<Like, Long> {

}
