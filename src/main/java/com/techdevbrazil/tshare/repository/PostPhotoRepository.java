package com.techdevbrazil.tshare.repository;

import com.techdevbrazil.tshare.enitity.PostPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostPhotoRepository extends JpaRepository<PostPhoto, Long> {

}
