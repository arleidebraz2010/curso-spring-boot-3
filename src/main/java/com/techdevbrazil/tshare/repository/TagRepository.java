package com.techdevbrazil.tshare.repository;

import com.techdevbrazil.tshare.enitity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {

}
